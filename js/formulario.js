const formulario = document.querySelector('#formulario-contacto');
const inputs = document.querySelectorAll('#formulario-contacto input');
const textarea = document.querySelector('#formulario-contacto textarea');
const botonenv = document.querySelector('#btnEnviar');


const expresiones = {
  nombre: /^[a-zA-ZñÑ\s]+$/,
  email: /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/,
  mensaje: /^[0-9a-zA-ZñÑ\s]+$/
};

const campos = {
  nombre: false,
  email: false,
  mensaje: false
};

const validarFormularioInput = (e) => {
  //Accedemos al valor que tiene el id
  //console.log(e.target.id);

  switch(e.target.id){
    case 'nombre':
      validarCampoInput(expresiones.nombre, e.target, 'nombre');
      console.log('input name');
      break;
    case 'email':
      validarCampoInput(expresiones.email, e.target, 'email');
      break;
  }
};

const validarCampoInput = (expresion, input, campo) => {
  //Hacemos una evaluacion de la expresion regular
  //Con value accedemos al input

  if(expresion.test(input.value)){
    //Remueve la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.remove('form-control-error');
  console.log('pasiiiii');

    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.remove('formulario-input-error-activo');
    campos[campo] = true;
  console.log('pasoinput');

  } else {
    //Adiciona la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.add('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.add('formulario-input-error-activo');
    campos[campo] = false;
  }
};

const validarFormularioTextArea = (e) => {
  //console.log(e.target.id);

  if(expresiones.mensaje.test(e.target.value)){
    //Remueve la clase
    document.querySelector('#grupo-mensaje #mensaje').classList.remove('form-control-error');
    document.querySelector('#grupo-mensaje .formulario-input-error').classList.remove('formulario-input-error-activo');
    campos['mensaje'] = true
  } else {
    //Adiciona la clase
    document.querySelector('#grupo-mensaje #mensaje').classList.add('form-control-error');
    document.querySelector('#grupo-mensaje .formulario-input-error').classList.add('formulario-input-error-activo');
    campos['mensaje'] = false;
  }
};

inputs.forEach((input) => {
  //console.log(input);
  /*
  keyup cuando se presiona la tecla y se levanta el dedo, el evento se ejecuta
  */
  input.addEventListener('keyup', validarFormularioInput);
  console.log('pasooo por aca xq');
  /blur es disparado cuando un elemento ha perdido su foco/
  input.addEventListener('blur', validarFormularioInput);
});

textarea.addEventListener('keyup', validarFormularioTextArea);

textarea.addEventListener('blur', validarFormularioTextArea);
//--------------------------------
botonenv.addEventListener('click',  (e) => {

  if(campos.nombre && campos.email && campos.mensaje){


    document.querySelector('.formulario-mensaje-exito').classList.add('formulario-mensaje-exito-activo');
    //Limpiar todos los campos del formulario
    setTimeout(() => {
      document.querySelector('.formulario-mensaje-exito').classList.remove('formulario-mensaje-exito-activo');
    }, 2000);
    formulario.reset();
  }
 
});